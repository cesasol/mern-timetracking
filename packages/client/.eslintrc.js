/** @type {import('eslint').Linter.Config} */
const eslintConfig = {
  env: {
    browser: true,
  },
  globals: {
    'JSX': 'readonly'
  },
  extends: [
    'plugin:react/recommended',
    'airbnb-typescript',
    'plugin:import/typescript',
    'airbnb/hooks',
    'plugin:@typescript-eslint/recommended',
    'plugin:@typescript-eslint/recommended-requiring-type-checking',
    'prettier',
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
    },
    ecmaVersion: 12,
    sourceType: 'module',
  },
  settings: {
    react: {
      version: '17.0.1'
    }
  },
  plugins: ['react'],
  rules: {
    'no-use-before-define': 0,
    'import/prefer-default-export': 0,
    'import/no-default-export': 1,
  },
}
// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
module.exports = eslintConfig
