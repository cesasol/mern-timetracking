import { ErrorResponse } from '@cesasol/timetracking__types'
import React from 'react'
import { ApiErrorList } from './ApiErrorList'

type Props = {
  error?: ErrorResponse | null
}

export const ApiError: React.FC<Props> = ({ error }: Props) => {
  if (!error) {
    return null
  }
  return (
    <div>
      <p>Error {error.message}</p>
      <ApiErrorList errors={error.errors} />
    </div>
  )
}

ApiError.defaultProps = {
  error: null,
}
