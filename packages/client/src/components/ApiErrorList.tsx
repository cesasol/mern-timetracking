/* eslint-disable jsx-a11y/no-redundant-roles */
import { ErrorMap } from '@cesasol/timetracking__types'
import React from 'react'

type Props = {
  errors?: ErrorMap
}

export const ApiErrorList: React.FC<Props> = ({ errors = [] }: Props) => {
  if (errors.length === 0) {
    return null
  }
  return (
    <ul role="list" className="list-inside">
      {errors.map(([field, list]) => (
        <li key={field}>
          <strong>{field}</strong>
          <ul role="list" className="list-disc list-inside">
            {list.map((error) => (
              <li key={error}>{error}</li>
            ))}
          </ul>
        </li>
      ))}
    </ul>
  )
}

ApiErrorList.defaultProps = {
  errors: [],
}
