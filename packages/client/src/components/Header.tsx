import React from 'react'
import { useSelector } from 'react-redux'
import { Link } from 'react-router-dom'
import { RootState } from '../store'

export const Header: React.FC = () => {
  const user = useSelector((state: RootState) => state.auth.user)

  return (
    <header className="bg-gray-600 text-white p-2 shadow">
      <div className="container flex justify-between">
        <h1 className="font-bold">
          <Link to="/">Time Tracking App</Link>
        </h1>
        <div>
          {user ? (
            user.name
          ) : (
            <div className="flex space-x-2">
              <Link to="/auth/login">Login</Link>
              <Link to="/auth/register">Register</Link>
            </div>
          )}
        </div>
      </div>
    </header>
  )
}
