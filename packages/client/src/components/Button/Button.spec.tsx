/* eslint-disable @typescript-eslint/no-unsafe-member-access */
/* eslint-disable @typescript-eslint/no-unsafe-call */
import React from 'react'
import { create } from 'react-test-renderer';
import { Button } from './Button'

describe('<Button>', () => {
  test('Render defaults', () => {
      const button = create(<Button>Simple Button</Button>)

      expect(button.toJSON()).toMatchSnapshot();
      
      expect(button.root.findByProps({className: 'button'}).props.type).toBe('button')
  })
})

export {}
