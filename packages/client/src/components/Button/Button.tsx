import React, { PropsWithChildren } from 'react'
import './button.css'

type Props = PropsWithChildren<{ type?: 'button' | 'submit' | 'reset' }>

export const Button: React.FC<Props> = ({
  type = 'button',
  children,
}: Props) => (
  // eslint-disable-next-line react/button-has-type
  <button type={type} className="button">
    {children}
  </button>
)
