import {
  ErrorResponse,
  PaginatedResponse,
  SingleResponse,
} from '@cesasol/timetracking__types'
import { ApiError } from './error'

export class Api {
  $token?: string

  baseUrl = 'http://localhost:3001'

  constructor() {
    const token: string | null = localStorage.getItem('token')
    if (token) {
      this.$token = token
    }
  }

  set token(token: string | undefined) {
    this.$token = token
    if (this.$token) {
      localStorage.setItem('token', this.$token)
    } else {
      localStorage.removeItem('token')
    }
  }

  get token(): string | undefined {
    return this.$token
  }

  async fetch<T extends SingleResponse | PaginatedResponse>(
    method: 'POST' | 'PUT' | 'GET',
    url: string,
    body?: unknown
  ): Promise<T> {
    const fullUrl = new URL(url, this.baseUrl)
    const response = await fetch(fullUrl.href, {
      method,
      body: JSON.stringify(body),
      headers: this.headers,
      mode: 'cors',
      credentials: 'include',
    })
    let jsonResponse
    try {
      jsonResponse = (await response.json()) as T | ErrorResponse
    } catch (error) {
      throw new ApiError(response)
    }

    if (response.ok) {
      return jsonResponse as T
    }
    throw new ApiError(response, jsonResponse as ErrorResponse)
  }

  get headers(): Record<string, string> {
    const headers: Record<string, string> = {
      'Content-Type': 'application/json;charset=UTF-8',
      'Accepts-Type': 'application/json;charset=UTF-8',
      'Access-Control-Allow-Origin': '*',
    }
    if (this.token) {
      headers.authorization = `Bearer ${this.token}`
    }
    return headers
  }
}

export const api = new Api()
