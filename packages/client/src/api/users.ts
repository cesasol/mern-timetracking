import { api } from '.'
import { User, SingleResponse } from '../../../types'

export type UserLoginResponse = SingleResponse<User, { token: string }>

export type LoginPayload = {
  email: string
  password: string
}

export type RegisterPayload = LoginPayload & { name: string }

export const loginRequest = (
  payload: LoginPayload
): Promise<UserLoginResponse> =>
  api
    .fetch<UserLoginResponse>('POST', '/auth/login', payload)
    .then((response) => {
      api.token = response.meta.token
      return response
    })

export const logoutRequest = (): Promise<SingleResponse> =>
  api.fetch<SingleResponse>('POST', '/auth/logout').then((response) => {
    api.token = undefined
    return response
  })

export const registerRequest = (
  payload: RegisterPayload
): Promise<UserLoginResponse> =>
  api
    .fetch<UserLoginResponse>('POST', '/auth/register', payload)
    .then((response) => {
      api.token = response.meta.token
      return response
    })

export const profileRequest = (): Promise<UserLoginResponse> =>
  api.fetch<UserLoginResponse>('GET', '/auth/profile').then((response) => {
    api.token = response.meta.token
    return response
  })
