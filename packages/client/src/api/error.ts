import { ErrorResponse } from '@cesasol/timetracking__types'

export class ApiError extends Error {
  statusCode: number

  response?: ErrorResponse

  statusMessage: string

  constructor(response: Response, body?: ErrorResponse) {
    let message = response.statusText
    if (response.headers.get('Content-Type') !== 'application/json') {
      message = 'Malformed response'
    }
    super(message)
    this.statusCode = response.status
    this.response = body
    this.statusMessage = response.statusText
  }
}
