import { ErrorResponse, MachineState, User } from '../../../../types'

export type AuthState = {
  user: null | User
  token: null | string
  loggedInAt: null | Date
  loading: MachineState
  error: null | ErrorResponse
}
export const LOGIN_USER = 'LOGIN_USER'
export const LOGOUT_USER = 'LOGIN_USER'

export type LoginActionType = {
  type: typeof LOGIN_USER
  payload: {
    user: User
    token: string
  }
}
export type LogoutActionType = {
  type: typeof LOGOUT_USER
}

export type AuthActionsType = LogoutActionType | LoginActionType
