/* eslint-disable no-param-reassign */
import {
  createAsyncThunk,
  createSlice,
  PayloadAction,
  SerializedError,
} from '@reduxjs/toolkit'
import { ErrorResponse, User } from '@cesasol/timetracking__types'
import { ApiError } from '../../api/error'
import {
  LoginPayload,
  loginRequest,
  logoutRequest,
  RegisterPayload,
  registerRequest,
  profileRequest,
} from '../../api/users'
import { AuthState } from './types'
import { api } from '../../api'

type ActionPayloadUser = {
  user: User
  token: string
}

export const login = createAsyncThunk<
  ActionPayloadUser,
  LoginPayload,
  { rejectValue: ErrorResponse }
>('auth/login', (payload: LoginPayload, { rejectWithValue }) =>
  loginRequest(payload)
    .then((response) => ({
      user: response.data,
      token: response.meta.token,
    }))
    .catch((_error) => {
      const error = _error as ApiError
      if (!error.response) {
        throw error
      }
      return rejectWithValue(error.response)
    })
)

export const logout = createAsyncThunk<
  unknown,
  null,
  { rejectValue: ErrorResponse }
>('auth/logout', (_payload, { rejectWithValue }) =>
  logoutRequest().catch((_error) => {
    const error = _error as ApiError
    if (!error.response) {
      throw error
    }
    return rejectWithValue(error.response)
  })
)

export const register = createAsyncThunk<
  ActionPayloadUser,
  RegisterPayload,
  {
    rejectValue: ErrorResponse
  }
>('auth/register', async (payload, { rejectWithValue }) => {
  try {
    const response = await registerRequest(payload)
    return {
      user: response.data,
      token: response.meta.token,
    }
  } catch (_error) {
    const error = _error as ApiError
    if (!error.response) {
      throw error
    }
    return rejectWithValue(error.response)
  }
})

export const getProfile = createAsyncThunk<
  { user: User | null; token: string | null },
  void,
  { rejectValue: null | ErrorResponse }
>('auth/profile', async (_arg, { rejectWithValue }) => {
  if (!api.token) {
    return { user: null, token: null }
  }
  try {
    const response = await profileRequest()
    return {
      user: response.data,
      token: response.meta.token,
    }
  } catch (_error) {
    const error = _error as ApiError
    if (!error.response) {
      throw error
    }
    if (error.statusCode !== 403) {
      return rejectWithValue(error.response)
    }
    return rejectWithValue(null)
  }
})

const initialState: AuthState = {
  user: null,
  token: null,
  loggedInAt: null,
  loading: 'idle',
  error: null,
}

const pendingReducer = (state: AuthState) => {
  if (state.loading !== 'pending') {
    state.loading = 'pending'
  }
}

const fulfilledReducer = (
  state: AuthState,
  action: PayloadAction<ActionPayloadUser>
) => {
  if (state.loading === 'pending') {
    state.loading = 'fulfilled'
    state.user = action.payload.user
    state.token = action.payload.token
  }
}
const rejectedReducer = (
  state: AuthState,
  action: PayloadAction<
    ErrorResponse | undefined,
    string,
    {
      arg: RegisterPayload | LoginPayload
      requestId: string
      rejectedWithValue: boolean
      requestStatus: 'rejected'
      aborted: boolean
      condition: boolean
    },
    SerializedError
  >
) => {
  if (state.loading === 'pending') {
    state.loading = 'rejected'
    if (action.payload) {
      state.error = action.payload
    } else {
      const error = action.error as ApiError
      state.error = {
        code: error.statusCode,
        message: error.message,
      }
    }
  }
}

export const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    reset: (state) => {
      if (state.loading === 'rejected') {
        state.loading = 'idle'
      }
    },
  },
  extraReducers: (builder) => {
    // Login cases
    builder.addCase(login.pending, pendingReducer)
    builder.addCase(login.fulfilled, fulfilledReducer)
    builder.addCase(login.rejected, rejectedReducer)
    // Register
    builder.addCase(register.pending, pendingReducer)
    builder.addCase(register.fulfilled, fulfilledReducer)
    builder.addCase(register.rejected, rejectedReducer)
    // Profile
    builder.addCase(getProfile.pending, pendingReducer)
    builder.addCase(getProfile.fulfilled, (state, action) => {
      if (state.loading === 'pending') {
        state.loading = 'idle'
        state.user = action.payload.user
        state.token = action.payload.token
      }
    })
    builder.addCase(getProfile.rejected, (state, action) => {
      if (state.loading === 'pending') {
        state.loading = 'rejected'
        state.user = null
        state.token = null
        api.token = undefined
        if (action.payload) {
          state.error = action.payload
        } else if (action.error) {
          const error = action.error as ApiError
          state.error = {
            code: error.statusCode,
            message: error.message,
          }
        }
      }
    })
    // Logout
    builder.addCase(logout.pending, pendingReducer)
    builder.addCase(logout.fulfilled, (state) => {
      if (state.loading === 'pending') {
        state.loading = 'fulfilled'
        state.user = null
        state.token = null
      }
    })
    builder.addCase(logout.rejected, (state, action) => {
      if (state.loading === 'pending') {
        state.loading = 'rejected'
        state.user = null
        state.token = null
        if (action.payload) {
          state.error = action.payload
        } else {
          const error = action.error as ApiError
          state.error = {
            code: error.statusCode,
            message: error.message,
          }
        }
      }
    })
  },
})

export const authReducer = authSlice.reducer
export const { reset } = authSlice.actions
