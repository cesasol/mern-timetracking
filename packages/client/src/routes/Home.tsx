import React from 'react'
import { useSelector } from 'react-redux'
import { RootState } from '../store'

export const Home: React.FC = () => {
  const user = useSelector((state: RootState) => state.auth.user)

  return (
    <main className="py-14 p-8">
      <div className="container">
        <h1>Welcome {user?.name}</h1>
        <pre className="p-2 font-mono text-sm max-h-60 overflow-auto w-full">
          {JSON.stringify(user)}
        </pre>
      </div>
    </main>
  )
}
