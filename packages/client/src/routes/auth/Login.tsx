/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { FormEvent, useState } from 'react'
import { useSelector } from 'react-redux'
import { ApiError } from '../../components/ApiError'
import { Button } from '../../components/Button/Button'
import { LayoutBoxed } from '../../Layouts/Boxed'
import { RootState, useAppDispatch } from '../../store'
import { login, reset } from '../../store/auth/reducers'

export const AuthLogin: React.FC = () => {
  const dispatch = useAppDispatch()
  const [email, updateEmail] = useState('')
  const [password, updatePassword] = useState('')
  const { loading, error } = useSelector((state: RootState) => state.auth)
  const submit = async (event: FormEvent) => {
    event.preventDefault()
    await dispatch(login({ email, password }))
  }
  const emailId = 'email'
  switch (loading) {
    case 'idle':
      return (
        <LayoutBoxed onSubmit={submit} tag="form">
          <h1 className="font-bold mb-4 text-xl text-center">Login</h1>
          <label htmlFor={emailId} className="block font-semibold my-2">
            username/email
          </label>
          <input
            type="email"
            id={emailId}
            name="email"
            className="block"
            value={email}
            onChange={(event) => updateEmail(event.target.value)}
          />
          <label htmlFor="password" className="block font-semibold my-2">
            Password
          </label>
          <input
            type="password"
            name="password"
            id="password"
            className="block"
            value={password}
            onChange={(event) => updatePassword(event.target.value)}
          />
          <Button type="submit">Login</Button>
        </LayoutBoxed>
      )
    case 'pending':
      return (
        <LayoutBoxed>
          <p>Loading</p>
        </LayoutBoxed>
      )
    case 'rejected':
      return (
        <LayoutBoxed className="bg-red-300 text-red-800">
          <ApiError error={error} />
          <button
            type="button"
            className="button"
            onClick={() => dispatch(reset())}
          >
            Try Again
          </button>
        </LayoutBoxed>
      )
    case 'fulfilled':
      return (
        <LayoutBoxed className="bg-green-300 text-green-800">
          You are in
        </LayoutBoxed>
      )
    default:
      return (
        <LayoutBoxed className="bg-red-300 text-red-800">
          <p className="text-red-600">Invalid error</p>
        </LayoutBoxed>
      )
  }
}
