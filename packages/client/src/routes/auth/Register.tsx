/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { FormEvent, useState } from 'react'
import { useSelector } from 'react-redux'
import { ApiError } from '../../components/ApiError'
import { LayoutBoxed } from '../../Layouts/Boxed'
import { RootState, useAppDispatch } from '../../store'
import { register, reset } from '../../store/auth/reducers'

export const AuthRegister: React.FC = () => {
  const dispatch = useAppDispatch()
  const [email, updateEmail] = useState('')
  const [password, updatePassword] = useState('')
  const [name, updateName] = useState('')
  const { loading, error } = useSelector((state: RootState) => state.auth)
  const submit = async (event: FormEvent) => {
    event.preventDefault()
    await dispatch(register({ email, password, name }))
  }
  switch (loading) {
    case 'idle':
      return (
        <LayoutBoxed onSubmit={submit} tag="form">
          <h1 className="text-xl font-bold mb-4">Registrate</h1>
          <label htmlFor="name" className="block font-semibold my-2">
            Name
          </label>
          <input
            type="text"
            name="name"
            id="name"
            className="block"
            value={name}
            onChange={(event) => updateName(event.target.value)}
          />
          <label htmlFor="email" className="block font-semibold my-2">
            email
          </label>
          <input
            type="email"
            id="email"
            name="email"
            className="block"
            value={email}
            onChange={(event) => updateEmail(event.target.value)}
          />
          <label htmlFor="password" className="block font-semibold my-2">
            Password
          </label>
          <input
            id="password"
            type="password"
            name="password"
            className="block"
            value={password}
            onChange={(event) => updatePassword(event.target.value)}
          />
          <button className="button" type="submit">
            Send
          </button>
        </LayoutBoxed>
      )
    case 'pending':
      return (
        <LayoutBoxed>
          <p>Loading</p>
        </LayoutBoxed>
      )
    case 'rejected':
      return (
        <LayoutBoxed className="bg-red-300 text-red-800">
          <ApiError error={error} />
          <button
            type="button"
            className="button"
            onClick={() => dispatch(reset())}
          >
            Try Again
          </button>
        </LayoutBoxed>
      )
    case 'fulfilled':
      return (
        <LayoutBoxed className="bg-green-300 text-green-800">
          You are in
        </LayoutBoxed>
      )
    default:
      return (
        <LayoutBoxed className="bg-red-300 text-red-800">
          <p className="text-red-600">Invalid error</p>
        </LayoutBoxed>
      )
  }
}
