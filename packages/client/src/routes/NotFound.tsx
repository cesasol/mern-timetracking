import React from 'react'

export const NotFound: React.FC = () => (
  <main>
    <h1>Not Found </h1>
  </main>
)
