import React from 'react'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import { Header } from './components/Header'
import { Home } from './routes/Home'
import { AuthLogin } from './routes/auth/Login'
import { NotFound } from './routes/NotFound'
import { AuthRegister } from './routes/auth/Register'
import { useAppDispatch } from './store'
import { getProfile } from './store/auth/reducers'

export const App: React.FC = () => {
  const dispatch = useAppDispatch()
  dispatch(getProfile())
    .then((state) => console.log(state))
    .catch((e) => console.error(e))

  return (
    <Router>
      <Header />
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/auth/login" component={AuthLogin} />
        <Route path="/auth/register" component={AuthRegister} />
        <Route path="*" component={NotFound} />
      </Switch>
    </Router>
  )
}
