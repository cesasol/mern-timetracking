import React, { FormEvent } from 'react'

type Props = React.PropsWithChildren<{
  tag?: string
  className?: string
  onSubmit?: (event: FormEvent) => void
}>

export const LayoutBoxed: React.FC<Props> = ({
  tag = 'div',
  className = '',
  onSubmit: submit = () => {},
  children,
}: Props) => {
  const Box = tag as keyof JSX.IntrinsicElements
  return (
    <main className="flex items-center justify-center bg-gray-900">
      <div className="container min-w-full">
        <Box
          className={`${className} shadow bg-white max-w-md mx-auto rounded flex flex-col p-4`}
          onSubmit={submit}
        >
          {children}
        </Box>
      </div>
    </main>
  )
}
