/* eslint-disable @typescript-eslint/ban-types */
export type Model = {
  _id?: string
  schemaversion: number
}

export type User = Model & {
  name: string
  email: string
  preferences: Record<string, string | number | boolean>
}

export type Project = Model & {
  name: string
  leader: User | string
  client: Client
}

export type TimeEntry = Model & {
  project: Project
  user: User
  createdAt: Date
  date: Date
  duration: number
  label: string
  task: string
}

export type Contact = {
  name: string
  phone: string
  email: string
}

export type Client = {
  name: string
  contact: Contact[]
}

export type PaginatedResponse<
  T extends unknown = null,
  M extends Record<string, unknown> = {}
> = {
  data: T[]
  pagination?: {
    current: number
    total: number
    shown: number
    entries: number
  }
  meta: M & { collection: string }
}

export type SingleResponse<
  T extends unknown = {},
  M extends Record<string, unknown> = {}
> = {
  data: T
  meta: M & { collection: string }
}

export type ErrorMap = [string, string[]][]

export type ErrorResponse = {
  message: string
  code: number
  errors?: ErrorMap
}

export type MachineState = 'idle' | 'pending' | 'rejected' | 'fulfilled'
