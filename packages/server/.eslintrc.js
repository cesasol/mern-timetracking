/** @type {import('eslint').Linter.Config} */
const eslintConfig = {
  env: {
    es6: true,
    node: true,
    browser: false,
  },
  rules: {
    'no-console': 0,
    'import/prefer-default-export': 0,
    'import/no-default-export': 1,
    'no-underscore-dangle': 0,
  },
}
// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
module.exports = eslintConfig
