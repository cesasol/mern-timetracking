export const HOST_NAME_DEV = 'http://localhost'
export const HOST_PORT_DEV = '3000'
export const SERVER_PORT_DEV = '3001'
export const HOST_URL_DEV = `${HOST_NAME_DEV}:${HOST_PORT_DEV}`
export const HOST_NAME_PROD = 'https://tracking.cesasol.com.mx'
export const HOST_ISSUER = 'https://cesasol.com.mx'
export const HOST_PORT_PROD = process.env.HOST_PORT_PROD || '80'
export const SERVER_PORT_PROD = HOST_PORT_PROD

let url: string
if (HOST_PORT_PROD === '80') {
  url = HOST_NAME_PROD
} else {
  url = `${HOST_NAME_PROD}:${HOST_PORT_PROD}`
}
export const HOST_URL_PROD = url
export const CORS_WHITELIST: string[] = [
  // Add the origins you would like to whitelist
  HOST_URL_DEV,
]
