import { SchemaDefinitionProperty } from 'mongoose'

export const currentVersion: SchemaDefinitionProperty = {
  type: Number,
  default: 0,
}
