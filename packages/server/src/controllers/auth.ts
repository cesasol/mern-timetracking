import { RequestHandler } from 'express'
import * as argon2 from 'argon2'
import jwt from 'jsonwebtoken'
import {
  ErrorResponse,
  SingleResponse,
  User,
} from '@cesasol/timetracking__types'
import { UserDocument, UserModel } from '../models/User'
import { SESSION_SECRET } from '../util/secrets'
import { HOST_ISSUER } from '../Hosts'

const generateToken = (
  user: UserDocument,
  sessionID: string
): Promise<string> =>
  new Promise((resolve, reject) => {
    jwt.sign(
      { id: user.id as string, email: user.email },
      SESSION_SECRET,
      {
        issuer: HOST_ISSUER,
        jwtid: sessionID,
        expiresIn: '1day',
      },
      (error, encoded) => {
        if (error) {
          reject(error)
        } else if (encoded) {
          resolve(encoded)
        } else {
          reject(new Error('No token'))
        }
      }
    )
  })

export const login: RequestHandler<
  Record<string, unknown>,
  ErrorResponse | SingleResponse<User, { token: string }>,
  { email: string; password: string }
> = async (req, res) => {
  const { email, password } = req.body
  let user: UserDocument | null = null
  try {
    user = await UserModel.findOne({ email }).exec()
  } catch (_error) {
    const error = _error as Error
    return res.status(500).json({
      message: error.message,
      code: 500,
    })
  }
  if (!user) {
    return res.status(403).json({
      message: 'Not Found',
      code: 0,
      errors: [['email', [`User with email: ${email} was not found`]]],
    })
  }

  let verified = false
  try {
    verified = await argon2.verify(user.password, password)
  } catch (error) {
    console.error(error)
    return res.status(500).json({
      message: 'There was an error with the authenticator service',
      code: 500,
    })
  }
  if (verified) {
    return res.json({
      data: user.toObject(),
      meta: {
        token: await generateToken(user, req.session.id),
        collection: 'Users',
      },
    })
  }

  return res.status(403).json({
    message: "Password doesn't match",
    code: 0,
    errors: [['password', ['Invalid password']]],
  })
}

export const logout: RequestHandler = (req, res) => {
  res.json(null)
}

export const register: RequestHandler<
  Record<string, unknown>,
  ErrorResponse | SingleResponse<User, { token: string }>,
  { email: string; password: string; name: string }
> = async (req, res) => {
  const { email, name, password } = req.body
  let user = await UserModel.findOne({ email }).exec()
  if (user) {
    return res.status(402).json({
      message: 'User already exists',
      code: 0,
      errors: [['email', ['A user with this email is already registered']]],
    })
  }
  try {
    user = new UserModel({
      email,
      password,
      name,
    })
    await user.save()
  } catch (_error) {
    const error = _error as Error
    return res.status(500).json({
      message: error.message,
      code: 0,
    })
  }
  return res.json({
    data: user.toObject(),
    meta: {
      token: await generateToken(user, req.session.id),
      collection: 'Users',
    },
  })
}

export const profile: RequestHandler = async (req, res) => {
  const { user } = req
  if (!user) {
    return res.status(500).json({
      message: 'Not user found in session',
      code: 500,
    })
  }
  return res.json({
    data: user.toObject(),
    meta: {
      token: await generateToken(user, req.session.id),
      collection: 'Users',
    },
  })
}
