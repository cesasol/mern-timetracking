import {
  Contact,
  ErrorResponse,
  PaginatedResponse,
  Project,
  SingleResponse,
} from '@cesasol/timetracking__types'
import { RequestHandler } from 'express'
import { ProjectModel } from '../models/Project'
import { UserDocument } from '../models/User'

export const getAllProjects: RequestHandler<
  Record<string, never>,
  PaginatedResponse<Project>,
  Record<string, never>,
  { page?: number }
> = async (req, res) => {
  const perPage = 15
  const current = req.query.page || 1
  const projects = await ProjectModel.find()
    .skip((current - 1) * perPage)
    .limit(perPage)
    .sort('updatedOn')
    .exec()
  const total = await ProjectModel.estimatedDocumentCount().exec()
  return res.json({
    data: projects.map((project) => project.toObject()),
    meta: {
      collection: 'Projects',
    },
    pagination: {
      current,
      total,
      shown: perPage,
      entries: projects.length,
    },
  })
}
export const getProject: RequestHandler<
  { id: string },
  SingleResponse<Project> | ErrorResponse,
  Record<string, never>
> = async (req, res) => {
  const projectId = req.params.id
  const project = await ProjectModel.findOne({ _id: projectId }).exec()
  if (!project) {
    res.status(404).json({
      message: 'Project not found',
      code: 404,
    })
    return
  }
  res.status(200).json({
    data: project.toObject(),
    meta: {
      collection: 'Projects',
    },
  })
}
export const createProject: RequestHandler<
  Record<string, never>,
  SingleResponse<Project> | ErrorResponse,
  {
    name: string
    client: {
      name: string
      contact: Contact[]
    }
  }
> = async (req, res) => {
  const { name, client } = req.body
  const user = req.user as UserDocument
  let project = new ProjectModel()
  project.client = client
  project.name = name
  project.leader = user.id as string
  project = await project.save()
  res.json({
    data: project.toObject(),
    meta: {
      collection: 'Projects',
    },
  })
}
