import express, { Response, Request, NextFunction } from 'express'
import cors from 'cors'
import compression from 'compression'
import session from 'express-session'
import mongo from 'connect-mongo'
import mongoose from 'mongoose'
import errorHandler from 'errorhandler'

import {
  MONGODB_URI,
  SESSION_SECRET,
  SERVER_PORT,
  ORIGIN_URI,
  ENVIRONMENT,
} from './util/secrets'

import { authRoutes } from './routes/auth'

// API keys and Passport configuration
import { CORS_WHITELIST } from './Hosts'

const MongoStore = mongo(session)
const mongoUrl: string = MONGODB_URI as string
mongoose.set('useNewUrlParser', true)
mongoose.set('useFindAndModify', false)
mongoose.set('useCreateIndex', true)
mongoose
  .connect(mongoUrl, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log('  MongoDB is connected successfully.')
  })
  .catch((err: Error) => {
    console.error(
      `  MongoDB connection error. Please make sure MongoDB is running. ${err.message}`
    )
    process.exit()
  })

// Express configuration
export const app = express()
app.set('server_port', SERVER_PORT)
app.set('origin_uri', ORIGIN_URI)
app.use(compression())
app.use(
  cors({
    origin: (
      requestOrigin: string | undefined,
      callback: (err: Error | null, allow?: boolean) => void
    ): void => {
      // allow requests with no origin
      if (requestOrigin && CORS_WHITELIST.indexOf(requestOrigin) === -1) {
        const message =
          "The CORS policy for this origin doesn't allow access from the particular origin."
        return callback(new Error(message), false)
      }
      // tslint:disable-next-line:no-null-keyword
      return callback(null, true)
    },
    credentials: true,
  })
)
app.use(express.json())
app.use(express.urlencoded({ extended: true }))
app.use(
  session({
    resave: true,
    saveUninitialized: true,
    secret: SESSION_SECRET,
    cookie: {
      domain: ORIGIN_URI,
      maxAge: 60 * 24 * 1000,
      sameSite: ENVIRONMENT === 'production' ? 'strict' : 'lax',
      secure: true,
    },
    store: new MongoStore({
      url: mongoUrl,
      autoReconnect: true,
    }),
  })
)
app.use((req: Request, res: Response, next: NextFunction) => {
  console.log(
    `[${req.method} ${req.originalUrl}] is called, body is ${JSON.stringify(
      req.body
    )}`
  )
  next()
})
app.use((req: Request, res: Response, next: NextFunction) => {
  res.locals.user = req.user
  next()
})
if (process.env.NODE_ENV === 'development') {
  app.use(errorHandler())
}

// Routes
app.use('/auth', authRoutes) // Auth client routes
