import { Request, RequestHandler } from 'express'
import jwt from 'jsonwebtoken'
import { UserModel } from '../models/User'
import { HTTP_CODES } from '../util/httpcodes'
import { SESSION_SECRET } from '../util/secrets'
import { HOST_ISSUER } from '../Hosts'

export const loggedInMiddleware: RequestHandler = async (
  req: Request,
  res,
  next
) => {
  const [, , token] = req.headers.authorization?.match(/^(Bearer)\s(.*)$/) || []
  if (!token) {
    res.status(HTTP_CODES.FORBIDDEN).json({
      message: 'Missing token',
      code: 0,
    })
    return
  }

  let userData
  try {
    userData = jwt.verify(token, SESSION_SECRET, {
      issuer: [HOST_ISSUER],
      // jwtid: req.session.id,
    }) as { token: string; id: string }
  } catch (_error) {
    const error = _error as Error
    console.error(error)
    res.status(403).json({
      message: error.message,
    })
    return
  }
  const user = await UserModel.findOne({
    token: userData.token,
    _id: userData.id,
  }).exec()
  if (!user) {
    res.status(HTTP_CODES.UNAUTHORIZED).json({
      message: 'You have to relogin',
      code: 0,
    })
    return
  }
  console.log(user)
  req.user = user
  next()
}
