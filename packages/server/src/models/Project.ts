import { Project } from '@cesasol/timetracking__types'
import mongoose, { Document, Schema } from 'mongoose'
import { currentVersion } from '../config/schemas'
import { clientSchema } from './Client'

export interface ProjectDocument extends Document<string>, Project {}

export const ProjectSchema = new Schema<ProjectDocument>(
  {
    version: currentVersion,
    name: { type: String, required: true },
    leader: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
      required: true,
    },
    client: clientSchema,
  },
  { timestamps: true }
)
export const ProjectModel = mongoose.model('Project', ProjectSchema)
