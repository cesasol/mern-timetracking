import mongoose, { Document } from 'mongoose'
import { Client as BaseClient } from '@cesasol/timetracking__types'
import { currentVersion } from '../config/schemas'

export interface ClientDocument extends Document<string>, BaseClient {}

export const clientSchema = new mongoose.Schema<ClientDocument>(
  {
    version: currentVersion,
    name: { type: String, unique: true },
    contact: [{ name: String, phone: String, email: String }],
  },
  { id: true }
)
