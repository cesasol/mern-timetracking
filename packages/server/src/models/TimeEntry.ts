import { TimeEntry } from '@cesasol/timetracking__types'
import mongoose, { Schema, Document, SchemaDefinition, Types } from 'mongoose'
import { currentVersion } from '../config/schemas'

export type TimeEntryDocument = Document<string> & TimeEntry

export const TimeEntrySchema = new Schema<TimeEntryDocument>(
  {
    schemaversion: currentVersion,
    task: { type: String, required: true },
    date: { type: Date, required: true },
    user: { type: Types.ObjectId, required: true, ref: 'Users' },
    project: { type: Types.ObjectId, required: true, ref: 'Project' },
    duration: { type: Number, required: true },
    label: { type: String, required: true },
  } as SchemaDefinition,
  { timestamps: true, id: true }
)

export const TimeEntryModel = mongoose.model<TimeEntryDocument>(
  'TimeEntries',
  TimeEntrySchema
)
