import mongoose, { Document, Schema, SchemaDefinition } from 'mongoose'
import * as argon2 from 'argon2'
import { User as BaseUser } from '@cesasol/timetracking__types'
import { currentVersion } from '../config/schemas'

type User = BaseUser & {
  password: string
  token?: string
}

export type UserDocument = Document<string> & User

export const userSchema = new Schema<UserDocument>(
  {
    schemaversion: currentVersion,
    email: {
      type: 'String',
      unique: true,
      required: true,
    },
    password: { type: 'String', required: true },
    name: { type: 'String', required: true },
    preferences: {
      type: 'Map',
      of: String,
    },
    token: {
      type: 'String',
      trim: true,
      index: true,
      unique: true,
      sparse: true,
    },
  } as SchemaDefinition,
  {
    timestamps: true,
    id: true,
  }
)

/**
 * Password hashing & Signing Url middleware.
 */
userSchema.pre('save', function save(next): void {
  // email cannot have capital character
  if (this.email) {
    this.email = this.email.toLowerCase()
  }
  if (!this.isModified('password')) {
    next()
    return
  }
  argon2
    .hash(this.password, {
      type: argon2.argon2id,
    })
    .then((hashedPassword) => {
      this.password = hashedPassword
      next()
    })
    .catch((error) => {
      next(error)
    })
})

export const UserModel = mongoose.model('Users', userSchema)
