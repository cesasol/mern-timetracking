import express, { RequestHandler, Router } from 'express'
import { check, validationResult } from 'express-validator'
import * as controllers from '../controllers/auth'
import { loggedInMiddleware } from '../middleware/auth'

const validationMiddleware: RequestHandler = (req, res, next) => {
  const errors = validationResult(req)
  if (!errors.isEmpty()) {
    const errorsArray = errors.array()
    const errorsMap = new Map() as Map<string, string[]>
    errorsArray.forEach(({ param, msg }) => {
      if (errorsMap.has(param)) {
        errorsMap.get(param)?.push(msg)
      } else {
        errorsMap.set(param, [msg])
      }
    })
    res.status(400).json({
      message: 'There are problems with your input',
      code: 0,
      errors: Array.from(errorsMap.entries()),
    })
    return
  }
  next()
}

const auth: Router = express.Router()
auth
  .route('/login')
  .post(
    [
      check('email', 'The email field is required').notEmpty(),
      check('email', 'Must be a valid email').isEmail().normalizeEmail(),
      check('password', 'The password field is required').notEmpty().trim(),
    ],
    validationMiddleware,
    controllers.login
  )
auth.route('/logout').post([loggedInMiddleware, controllers.logout])
auth
  .route('/register')
  .post(
    [
      check('email', 'The emeail field is required').notEmpty(),
      check('email', 'Must be a valid email').normalizeEmail(),
      check('password', 'The password field is required').notEmpty().trim(),
      check('password').isLength({ min: 6 }),
      check('name').notEmpty().isLength({ min: 3 }),
    ],
    validationMiddleware,
    controllers.register
  )
auth.route('/profile').get([loggedInMiddleware, controllers.profile])
export const authRoutes = auth
