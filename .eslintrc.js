/** @type {import('eslint').Linter.Config} */
const eslintConfig = {
  root: true,
  env: {
    es6: true,
    node: true
  },
  extends: [
    'airbnb-typescript/base',
    'plugin:import/typescript',

    'plugin:@typescript-eslint/recommended',
    'plugin:@typescript-eslint/recommended-requiring-type-checking',

    'prettier',
  ],
  parserOptions: {
    ecmaVersion: 12,
    sourceType: 'module',
    project: [
      'tsconfig.eslint.json',
      'packages/*/tsconfig.json'
    ],
    tsconfigRootDir: __dirname,

  },

  plugins: ['react', 'jest', '@typescript-eslint', 'prettier'],
  rules: {
    'import/extensions': [
      'error',
      'always',
      {
        'd.ts': 'never',
        'ts': 'never',

      }
    ],
    'react/jsx-filename-extension': [1, { extensions: ['.tsx', '.jsx'] }],
    'no-use-before-define': 0,
    'import/prefer-default-export': 0,
    'import/no-default-export': 1,
  },
}
// eslint-disable-next-line @typescript-eslint/no-unsafe-assignment
module.exports = eslintConfig
